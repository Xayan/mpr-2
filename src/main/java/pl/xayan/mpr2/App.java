package pl.xayan.mpr2;

import pl.xayan.mpr2.db.*;
import pl.xayan.mpr2.domain.*;

public class App {
	private static PersonDbManager personDbManager;
	private static AddressDbManager addressDbManager;
	private static UserDbManager userDbManager;
	private static RoleDbManager roleDbManager;
	private static PermissionDbManager permissionDbManager;

	public static void main(String[] args) {
		personDbManager = new PersonDbManager();
		addressDbManager = new AddressDbManager();
		userDbManager = new UserDbManager();
		roleDbManager = new RoleDbManager();
		permissionDbManager = new PermissionDbManager();
		setupDatabase();
	}
	
	public static void setupDatabase() {
		Person person = new Person();
		person.setName("Jan");
		person.setSurname("Kowalski");
		person.setAge(30);
		personDbManager.addPerson(person);
		
		Address address = new Address();
		address.setStreet("Ul. Jakaśtam 4/20");
		address.setPostalCode("00-000");
		address.setCity("Gdańsk");
		address.setPhoneNumber("+48 123 456 789");
		addressDbManager.addAddress(address);
		
		User user = new User();
		user.setLogin("jan.kowalski");
		user.setPassword("123456");
		userDbManager.addUser(user);
		
		Role role = new Role();
		role.setName("Admin");
		roleDbManager.addRole(role);
		
		Permission permission = new Permission();
		permission.setName("delete");
		permissionDbManager.addPermission(permission);
		
		roleDbManager.addPermission(role, permission);
	
		userDbManager.addRole(user, role);

		personDbManager.addAddress(person, address);
		personDbManager.setUser(person, user);
	}
}
