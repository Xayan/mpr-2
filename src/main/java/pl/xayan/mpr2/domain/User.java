package pl.xayan.mpr2.domain;

import java.util.ArrayList;
import java.util.List;

public class User {
	public int id;
	private String login;
	private String password;
	private Person person;
	private List<Role> roles = new ArrayList<Role>();
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
}
