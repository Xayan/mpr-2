package pl.xayan.mpr2.domain;

import java.util.ArrayList;
import java.util.List;

public class Role {
	public int id;
	private String name;
	private List<Permission> permissions = new ArrayList<Permission>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
}
